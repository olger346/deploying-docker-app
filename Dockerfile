#Escojer una imagen de partida para el dockerfile
FROM alpine:3.5
#actualizar el paquete pip
RUN apk add --update py2-pip

#copiar los elementos de la aplicacion en este caso los directorios y archivos 
COPY dependencias.txt / 

#Ejecutar pip para instalar las dependencias, en este caso la version de flask
RUN pip install --no-cache-dir -r /dependencias.txt

#Lo siguiente es copiar el directorio templates y el directorio static para flask
COPY api.py /home/
COPY static/assets/css/main.css /home/static/assets/css/main.css
COPY static/assets/css/font-awesome.min.css /home/static/assets/css/font-awesome.min.css
COPY static/images/logo.svg /home/static/assets/images/logo.svg
COPY static/images/pic01.jpg /home/static/assets/images/pic01.jpg
COPY static/images/pic02.jpg /home/static/assets/images/pic02.jpg
COPY static/images/pic03.jpg /home/static/assets/images/pic03.jpg
COPY static/assets/js/jquery.min.js /home/static/assests/js/jquery.min.js
COPY static/assets/js/main.js /home/static/assets/js/browser.min.js
COPY static/assets/js/util.js /home/static/assets/js/util.js

COPY templates/index.html /home/templates/index.html
COPY templates/sha256.html /home/templates/sha256.html
#Exponer el puerto de la aplicacion
EXPOSE 5000

#Ejecutar 
CMD ["python", "/home/api.py"]


