from flask import Flask, request
from flask import render_template
from hashlib import sha256

app=Flask(__name__)

@app.route("/sha256", methods=['POST'])
def hash256():
    hash = sha256(request.form['cadena'].encode("utf-8")).hexdigest()
    return hash
@app.route("/")
def root():
    return render_template('index.html')

@app.route("/sha256.html")
def file1():
    return render_template("sha256.html")

app.run(host="0.0.0.0", port=5000)
